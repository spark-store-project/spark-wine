#/bin/bash
SHELL_DIR=$(dirname $(realpath $0))

source /opt/bashimport/transhell.sh
load_transhell_debug

until [ "$IS_CLOSE" = "1" ];do

CHOSEN_SETTINGS=`zenity --list \
	--width=700 \
	--height=350 \
       --title="${TRANSHELL_CONTENT_WELCOME_AND_CHOOSE_ONE_TO_RUN}" \
       --column="${TRANSHELL_CONTENT_OPTION}" \
     "${TRANSHELL_CONTENT_SET_APP}" \
	"${TRANSHELL_CONTENT_SET_GLOBAL_SCALE}" \
	"${TRANSHELL_CONTENT_SYNC_APP_SCALE_WITH_GLOBAL}" \
	"${TRANSHELL_CONTENT_ONLY_AVAILABLE_TO_SPARK_DWINE_HELPER_APP}" `

echo "$CHOSEN_SETTINGS"
case "$CHOSEN_SETTINGS" in 
	"${TRANSHELL_CONTENT_SET_GLOBAL_SCALE}")
########
	zenity --info --text="${TRANSHELL_CONTENT_THIS_WILL_NOT_TAKE_EFFECT_IN_DEEPIN_BECAUSE_READ_ENVIRONMENT_FIRST}" --width=500 --height=150

dimensions=`xdpyinfo | grep dimensions | sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/'`
scale_factor=`zenity --list \
	--width=700 \
	--height=350 \
       --title="${TRANSHELL_CONTENT_YOUR_DIMENSION_IS} $dimensions ${TRANSHELL_CONTENT_PLEASE_CHOOSE_ONE_BELOW}" \
       --column="${TRANSHELL_CONTENT_OPTION}" \
       1.0 \
       1.25 \
	1.5 \
	1.75 \
       2.0`

case "$scale_factor" in 
	"")
	zenity --info --text="${TRANSHELL_CONTENT_1_SCALE_AS_DEFAULT}" --width=500 --height=150
	scale_factor="1.0"
	;;
	*)
zenity --info --text="${TRANSHELL_CONTENT_SCALE_IS} $scale_factor ${TRANSHELL_CONTENT_SAVED}" --width=500 --height=150
	;;
esac
echo "$scale_factor" > $HOME/.config/spark-wine/scale.txt


	;;
########




	"${TRANSHELL_CONTENT_SET_APP}")
	bash "${SHELL_DIR}/wine-app-launcher.sh"
	;;
	"${TRANSHELL_CONTENT_SYNC_APP_SCALE_WITH_GLOBAL}")
	find ${HOME}/.deepinwine/ -name "scale.txt" -type f -print -exec rm -rf {} \;
	zenity --info --text="${TRANSHELL_CONTENT_BOTTLES_BELOW_HAVE_SYNCED_SCALE_WITH_GLOBAL}：\n`cd ${HOME}/.deepinwine/ && ls`" --width=500 --height=150
	;;
	"${TRANSHELL_CONTENT_ONLY_AVAILABLE_TO_SPARK_DWINE_HELPER_APP}")
	
	;;

	*)
	IS_CLOSE="1"
	;;


esac
done
