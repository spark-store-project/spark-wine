#/bin/bash
DEPEND=`dpkg -l | grep   fakeroot`
if [ "$DEPEND" = "" ] ; then 
echo "未安装依赖：fakeroot 本脚本退出"
exit 1
fi

if [ $# -lt 1 ]; then
echo "无参数，无法启动。用法 $0 版本号"
echo "No option detected, exit. Usage: $0 version"
exit 1
fi

version="$1"
######################################################
echo "build debian package"
mkdir -p  pkg/DEBIAN
cp -r ./s-wine-helper/* pkg/
SIZE=`du -s ./pkg`
SIZE=`echo ${SIZE%%.*}`

cat  << EOF >pkg/DEBIAN/control
Package: spark-dwine-helper
Version: $version
Architecture: all
Maintainer: shenmo <shenmo@spark-app.store>
Installed-Size: $SIZE
Depends: zenity, p7zip-legacy (>= 16.02+dfsg-8+Mejituu-2024022216) | p7zip-full (<< 16.02+transitional.1), fonts-noto-cjk, transhell, python3, wmctrl
Recommends: python3-pyqt5
Section: utils
Priority: extra
Multi-Arch: foreign
Provides: store.spark-app.spark-dwine-helper(=$version)
Replaces: store.spark-app.spark-dwine-helper(<=$version)
Homepage: https://gitee.com/deepin-community-store/spark-wine
Description: Spark Deepin Wine Helper


EOF


cp postinst pkg/DEBIAN/postinst
cp prerm pkg/DEBIAN/prerm

chmod +x pkg/DEBIAN/postinst
chmod +x pkg/DEBIAN/prerm
cd pkg && fakeroot dpkg-deb -Z xz -b . ../
cd ..

echo "普通deb包已经准备好，正在生成UOS deb包"
#################################################################



cat  << EOF >pkg/opt/apps/store.spark-app.spark-dwine-helper/info
{
    "appid": "store.spark-app.spark-dwine-helper",
    "name": "store.spark-app.spark-dwine-helper",
    "version": "$version",
    "arch": ["amd64,arm64"],
    "permissions": {
        "autostart": false,
        "notification": false,
        "trayicon": false,
        "clipboard": false,
        "account": false,
        "bluetooth": false,
        "camera": false,
        "audio_record": false,
        "installed_apps": false
    }
}



EOF

SIZE=`du -s ./pkg`
SIZE=`echo ${SIZE%%.*}`

cat  << EOF >pkg/DEBIAN/control
Package: store.spark-app.spark-dwine-helper
Version: $version
Architecture: all
Maintainer: shenmo <shenmo@spark-app.store>
Installed-Size: $SIZE
Depends: zenity, p7zip-legacy (>= 16.02+dfsg-8+Mejituu-2024022216) | p7zip-full (<< 16.02+transitional.1), fonts-noto-cjk, transhell, python3
Recommends: wmctrl,python3-pyqt5
Section: utils
Priority: extra
Provides: spark-dwine-helper(=$version)
Conflicts: spark-dwine-helper
Replaces: spark-dwine-helper(<=$version)
Multi-Arch: foreign
Homepage: https://gitee.com/deepin-community-store/spark-wine
Description: Spark Deepin Wine Helper



EOF
cd pkg && fakeroot dpkg-deb -Z xz -b . ../
cd ..

echo "UOS deb包已经准备好"
rm -rf pkg/


